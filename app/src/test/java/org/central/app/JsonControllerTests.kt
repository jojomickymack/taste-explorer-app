package org.central.app

import org.central.app.controllers.JsonController
import org.junit.Assert.assertEquals
import org.junit.Test


class JsonControllerTests {

    val json = JsonController()
    val autocompleteJsonString = """{
  "suggestions": [{
    "value": "The Peanuts Movie (2015 Movie)",
    "data": "The-Peanuts-Movie"
  }, {
    "value": "Peanuts (Book)",
    "data": "Peanuts"
  }, {
    "value": "Peanuts (TV Show)",
    "data": "Peanuts-TV-Show"
  }]
}"""

    // in reality each of these is really long
    val similarJsonString = """{
  "Similar": {
    "Info": [{
      "Name": "The Notorious B.I.G.",
      "Type": "music",
      "wTeaser": "\n\n\nChristopher George Latore Wallace...",
      "wUrl": "http://en.wikipedia.org/wiki/The_Notorious_B.I.G",
      "yUrl": "https://www.youtube-nocookie.com/embed/_JZom_gVfuw",
      "yID": "_JZom_gVfuw"
    }],
    "Results": [{
      "Name": "Tupac Shakur",
      "Type": "music",
      "wTeaser": "\n\nTupac Amaru Shakur...",
      "wUrl": "http://en.wikipedia.org/wiki/Tupac_Shakur",
      "yUrl": "https://www.youtube-nocookie.com/embed/Mb1ZvUDvLDY",
      "yID": "Mb1ZvUDvLDY"
    }, {
      "Name": "Nate Dogg",
      "Type": "music",
      "wTeaser": "Music & Me is the second studio album by American hip hop recording artist Nate Dogg...",
      "wUrl": "http://en.wikipedia.org/wiki/Music_and_Me_(Nate_Dogg_album)",
      "yUrl": "https://www.youtube-nocookie.com/embed/1plPyJdXKIY",
      "yID": "1plPyJdXKIY"
    }]
  }
}
"""


    val autocompleteResultsList = json.parseAutocomplete(autocompleteJsonString)
    val similarResultsList = json.parseSimilar(similarJsonString)

    @Test
    fun check_size() {
        assertEquals(3, autocompleteResultsList.size)
    }

    @Test
    fun check_peanuts_movie() {
        assertEquals(0, autocompleteResultsList[0].id)
        assertEquals("The Peanuts Movie (2015 Movie)", autocompleteResultsList[0].value)
        assertEquals("The-Peanuts-Movie", autocompleteResultsList[0].data)
    }

    @Test
    fun check_peanuts_book() {
        assertEquals(1, autocompleteResultsList[1].id)
        assertEquals("Peanuts (Book)", autocompleteResultsList[1].value)
        assertEquals("Peanuts", autocompleteResultsList[1].data)
    }

    @Test
    fun check_peanuts_show() {
        assertEquals(2, autocompleteResultsList[2].id)
        assertEquals("Peanuts (TV Show)", autocompleteResultsList[2].value)
        assertEquals("Peanuts-TV-Show", autocompleteResultsList[2].data)
    }

    /**
     * the similar json is a lot more complex
     */

    @Test
    fun check_rappers_size() {
        assertEquals(3, similarResultsList.size)
    }

    @Test
    fun check_notorious_big() {
        assertEquals(0, similarResultsList[0].id)
        assertEquals("The Notorious B.I.G.", similarResultsList[0].name)
        assertEquals("music", similarResultsList[0].type)
        assertEquals("_JZom_gVfuw", similarResultsList[0].yId)
    }

    @Test
    fun check_tupac() {
        assertEquals(1, similarResultsList[1].id)
        assertEquals("Tupac Shakur", similarResultsList[1].name)
        assertEquals("music", similarResultsList[1].type)
        assertEquals("Mb1ZvUDvLDY", similarResultsList[1].yId)
    }

    @Test
    fun check_nate_dogg() {
        assertEquals(2, similarResultsList[2].id)
        assertEquals("Nate Dogg", similarResultsList[2].name)
        assertEquals("music", similarResultsList[2].type)
        assertEquals("1plPyJdXKIY", similarResultsList[2].yId)
    }
}

