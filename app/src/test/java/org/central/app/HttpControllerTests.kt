package org.central.app

import org.central.app.controllers.HttpController
import org.central.app.objects.ItemAutocomplete
import org.junit.Test

import org.junit.Assert.*


class HttpControllerTests {

    val http = HttpController()

    /**
     * these are some apis on the internet that's just for messing with
     */

    @Test
    fun do_get_test_to_rest_api_example() {
        assertEquals("false", http.doGetRequest("http://dummy.restapiexample.com/api/v1/employee/1"))
    }

    @Test
    fun do_get_test_to_json_placeholder() {
        val jsonResponse = """{  "userId": 1,  "id": 1,  "title": "delectus aut autem",  "completed": false}"""
        assertEquals(jsonResponse, http.doGetRequest("https://jsonplaceholder.typicode.com/todos/1"))
    }

    /**
     * these were some tricky 'names' that gave me issues before because of punctuation in the expected name
     */

    @Test
    fun get_autocomplete_hitchhikers_guide() {
        assertEquals("The Hitchhiker's Guide To The Galaxy", http.getNameFromAutocomplete(ItemAutocomplete(9, "The Hitchhiker's Guide To The Galaxy (Book)", "The-Hitchhiker-s-Guide-To-The-Galaxy-Book")))
    }

    @Test
    fun get_autocomplete_notorius_big() {
        assertEquals("The Notorious B.I.G.", http.getNameFromAutocomplete(ItemAutocomplete(9, "The Notorious B.I.G. (Music)", "The-Notorious-B-I-G")))
    }

    /**
     * sanity for the url returned for autocomplete - there are some weird rules for punctuation and the things in parenthesis
     */

    @Test
    fun get_url_for_autocomplete_notorius_big() {
        assertEquals("https://tastedive.com/api/autocomplete?k=303248-coolapp0-7N4QE56Z&v=3&t=&target=search&q=The+Notorious+B.I.G.", http.getAutocompleteUrl(http.getNameFromAutocomplete(ItemAutocomplete(9, "The Notorious B.I.G. (Music)", "The-Notorious-B-I-G"))))
    }

    @Test
    fun get_url_for_autocomplete_hitch() {
        assertEquals("https://tastedive.com/api/autocomplete?k=303248-coolapp0-7N4QE56Z&v=3&t=&target=search&q=Hitch", http.getAutocompleteUrl(http.getNameFromAutocomplete(ItemAutocomplete(9, "Hitch (2005 Movie)", "Hitch"))))
    }

    @Test
    fun get_url_for_autocomplete_hitchhikers_guide() {
        assertEquals("https://tastedive.com/api/autocomplete?k=303248-coolapp0-7N4QE56Z&v=3&t=&target=search&q=The+Hitchhiker's+Guide+To+The+Galaxy", http.getAutocompleteUrl(http.getNameFromAutocomplete(ItemAutocomplete(9, "The Hitchhiker's Guide To The Galaxy (TV Show)", "The-Hitchhiker-s-Guide-To-The-Galaxy-TV-Show"))))
    }

    @Test
    fun get_url_for_autocomplete_mafia_game() {
        assertEquals("https://tastedive.com/api/autocomplete?k=303248-coolapp0-7N4QE56Z&v=3&t=&target=search&q=Mafia:+The+City+Of+Lost+Heaven", http.getAutocompleteUrl(http.getNameFromAutocomplete(ItemAutocomplete(9, "Mafia: The City Of Lost Heaven (Game)", "Mafia-The-City-Of-Lost-Heaven"))))
    }

    /**
     * same types of tests, this time for 'similar' endpoint
     */

    @Test
    fun get_url_for_similar_notorius_big() {
        assertEquals("https://tastedive.com/api/similar?k=303248-coolapp0-7N4QE56Z&info=1&q=The+Notorious+B.I.G.", http.getSimilarUrlFromName(http.getNameFromAutocomplete(ItemAutocomplete(9, "The Notorious B.I.G. (Music)", "The-Notorious-B-I-G"))))
    }

    @Test
    fun get_url_for_similar_hitch() {
        assertEquals("https://tastedive.com/api/similar?k=303248-coolapp0-7N4QE56Z&info=1&q=Hitch", http.getSimilarUrlFromName(http.getNameFromAutocomplete(ItemAutocomplete(9, "Hitch (2005 Movie)", "Hitch"))))
    }

    @Test
    fun get_url_for_similar_hitchhikers_guide() {
        assertEquals("https://tastedive.com/api/similar?k=303248-coolapp0-7N4QE56Z&info=1&q=The+Hitchhiker's+Guide+To+The+Galaxy", http.getSimilarUrlFromName(http.getNameFromAutocomplete(ItemAutocomplete(9, "The Hitchhiker's Guide To The Galaxy (TV Show)", "The-Hitchhiker-s-Guide-To-The-Galaxy-TV-Show"))))
    }

    @Test
    fun get_url_for_similar_mafia_game() {
        assertEquals("https://tastedive.com/api/similar?k=303248-coolapp0-7N4QE56Z&info=1&q=Mafia:+The+City+Of+Lost+Heaven", http.getSimilarUrlFromName(http.getNameFromAutocomplete(ItemAutocomplete(9, "Mafia: The City Of Lost Heaven (Game)", "Mafia-The-City-Of-Lost-Heaven"))))
    }

    /**
     * and the youtube url, really simple stuff
     */

    @Test
    fun get_url_for_youtube_image() {
        assertEquals("https://img.youtube.com/vi/testing/0.jpg", http.getYoutubeImageUrl("testing"))
    }
}
