package org.central.app

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

import org.central.app.pages.Splash
import org.central.app.sqliteSupport.FavoritesProvider
import android.content.Intent
import android.widget.ImageButton
import org.central.app.pages.core.Favorites
import org.central.app.pages.core.TasteSettings
import org.central.app.pages.core.Search
import org.central.app.services.MusicPlayingService

/**
 * I decided before starting the app that I wanted to have lots of fragments inside of just a few activities
 *
 * the way it turned out there's a 12 fragments and 2 activities. One of the activities (Welcome) doesn't really need to exist
 * anyway.
 *
 * this parent activity has a FrameLayout called 'parent_container' that exists beneath the toolbar, which is onmi-present and
 * contains the app navigation.
 *
 * the user will have the impression that there's a lot of different pages in the app, but it's really just this ParentActivity
 * with fragments being replaced in this 'parent_container' area.
 *
 * I learned a lot about layouts while preparing this app - mainly in regards to fragments, RecyclerViews, and AsyncTasks.
 */

class ParentActivity : AppCompatActivity() {

    lateinit var provider: FavoritesProvider

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // find the layout for this activity - it's a toolbar and a 'FrameLayout' called 'parent_container'
        setContentView(R.layout.parent_main)

        // find by buttons in the layout so I can attach onClickListeners to them
        val search = findViewById(R.id.parent_toolbar_search) as ImageButton
        val favorites = findViewById(R.id.parent_toolbar_favorites) as ImageButton
        val settings = findViewById(R.id.parent_toolbar_settings) as ImageButton

        // this method gets called when the screen is rotated or the app comes back into the foreground - don't do the
        // initial fragment trasaction unless you don't have a savedInstanceState!
        // without this, the first fragment would be added again and the music would restart when you rotate the screen!
        if (savedInstanceState == null) {
            // let's just make sure to instantiate the content provider - I probably don't need to do that
            provider = FavoritesProvider()
            // start the music using the MusicPlayingService
            startService(Intent(this, MusicPlayingService::class.java))
            // put the first fragment into the 'parent_container'
            supportFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.enter_from_top, R.anim.fade_out) // these animations are easy - it's in res/anim
                .add(R.id.parent_container, Splash())
                .commit()
        }

        // attach listeners to my buttons - I don't care if I have a savedInstanceState anymore
        search.setOnClickListener { v ->
            supportFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .replace(R.id.parent_container, Search())
                .addToBackStack(null)
                .commit()
        }

        favorites.setOnClickListener { v ->
            supportFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .replace(R.id.parent_container, Favorites())
                .addToBackStack(null)
                .commit()
        }

        settings.setOnClickListener { v ->
            supportFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .replace(R.id.parent_container, TasteSettings())
                .addToBackStack(null)
                .commit()
        }
    }
}
