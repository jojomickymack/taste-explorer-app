package org.central.app.services

import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.IBinder

import android.media.MediaPlayer
import android.util.Log
import org.central.app.R

/**
 * I really didn't feel like the app needed a service at all, and struggled to somehow combine what I was using AsyncTasks for
 * into a service.
 *
 * That didn't work at all because something that updates the ui will need to have the ui elements in scope - and a service is kind
 * of supposed to be away from the ui in the background perhaps
 */

class MusicPlayingService : Service() {

    // the MediaPlayer inside of the service does all the work, I only listen for signals to call pause() and start() on it
    lateinit var player: MediaPlayer

    // the uris that I use in the settings to send signals to this service
    val startAction = "org.central.app.Start"
    val stopAction = "org.central.app.Stop"

    var playing = false

    // inline initialization of the musicCommandReceiver
    var musicCommandReceiver: BroadcastReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            // kotlin has 'when' - it's similar to 'switch' - code doesn't need explanation
            when (intent.action) {
                startAction -> {
                    if (!playing) {
                        player.start()
                        playing = !playing
                    }
                }
                stopAction -> {
                    if (playing) {
                        player.pause()
                        playing = !playing
                    }
                }
            }
        }
    }

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        // load the ogg file which is in res/raw - some song I found on freesound
        player = MediaPlayer.create(this, R.raw.sweetclouds)
        player.isLooping = true
        // always seems too loud - turn it down
        player.setVolume(60f, 60f)

        // add my actions to the filter and register it
        val filter = IntentFilter()
        filter.addAction(startAction)
        filter.addAction(stopAction)
        registerReceiver(musicCommandReceiver, filter)
    }

    // start the music - do I need to put this in comments? the code is clear enough
    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        player.start()
        playing = true
        return START_NOT_STICKY
    }

    override fun onStart(intent: Intent, startId: Int) {
        Log.d("musicman", "music service onstart called")
    }

    override fun onDestroy() {
        player.stop()
        player.release()

        unregisterReceiver(musicCommandReceiver)
    }
}