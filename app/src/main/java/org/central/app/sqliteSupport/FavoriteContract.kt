package org.central.app.sqliteSupport

import android.content.ContentUris
import android.net.Uri
import android.provider.BaseColumns

/**
 * pretty much copied straight from 'HobbitProvider' and just changed field names
 *
 * the fields in the 'Contract' could be kept anywhere, it's just smart to put them all in here so it can
 * easily be reused for something else and you don't have to worry about accidentally doing a typo for a field name
 * as long as you use these
 */

object FavoriteContract {

    val CONTENT_AUTHORITY = "org.central.app.FavoritesProvider"
    val BASE_CONTENT_URI = Uri.parse("content://$CONTENT_AUTHORITY")
    val PATH_CHARACTER = "favorites"

    /*
     * Columns
     */

    /**
     * Inner class that defines the table contents of the Hobbit
     * table.
     */
    object FavoriteEntry : BaseColumns {

        /**
         * Use BASE_CONTENT_URI to create the unique URI for Acronym
         * Table that apps will use to contact the content provider.
         */
        val CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_CHARACTER).build()

        /**
         * When the Cursor returned for a given URI by the
         * ContentProvider contains 0..x items.
         */
        val CONTENT_ITEMS_TYPE = "vnd.android.cursor.dir/$CONTENT_AUTHORITY/$PATH_CHARACTER"

        /**
         * When the Cursor returned for a given URI by the
         * ContentProvider contains 1 item.
         */
        val CONTENT_ITEM_TYPE = "vnd.android.cursor.item/$CONTENT_AUTHORITY/$PATH_CHARACTER"

        /**
         * Name of the database table.
         */
        val TABLE_NAME = "favorites"

        /**
         * Columns to store data.
         */
        val _ID = "_id";
        val NAME = "name"
        val W_TEASER = "wTeaser"
        val Y_ID = "yId"
        val W_URL = "wUrl"
        val Y_URL = "yUrl"
        val DESCRIPTION = "description"

        /**
         * Columns to display.
         */
        val sColumnsToDisplay = arrayOf(
            _ID,
            NAME,
            W_TEASER,
            Y_ID,
            W_URL,
            Y_URL,
            DESCRIPTION
        )


        /**
         * Return a Uri that points to the row containing a given id.
         *
         * @param id row id
         * @return Uri URI for the specified row id
         */
        fun buildUri(id: Long): Uri {
            return ContentUris.withAppendedId(CONTENT_URI, id)
        }
    }
}
