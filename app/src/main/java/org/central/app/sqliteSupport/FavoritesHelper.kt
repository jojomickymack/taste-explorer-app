package org.central.app.sqliteSupport

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import java.io.File
import java.io.File.separator

/**
 * a SQLiteOpenHelper is needed even for apps that don't have a content provider but do use sqlite
 */

class FavoritesHelper(context: Context) : SQLiteOpenHelper(context, "${context.cacheDir}${separator}favorites", null, 1) {

    lateinit var db: SQLiteDatabase

    val CREATE_TABLE = ("""CREATE TABLE ${FavoriteContract.FavoriteEntry.TABLE_NAME}
(${FavoriteContract.FavoriteEntry._ID} INTEGER PRIMARY KEY,
${FavoriteContract.FavoriteEntry.NAME} TEXT,
${FavoriteContract.FavoriteEntry.W_TEASER} TEXT,
${FavoriteContract.FavoriteEntry.Y_ID} TEXT,
${FavoriteContract.FavoriteEntry.W_URL} TEXT,
${FavoriteContract.FavoriteEntry.Y_URL} TEXT,
${FavoriteContract.FavoriteEntry.DESCRIPTION} TEXT);""")

    /**
     * Constructor - initialize database name and version, but don't
     * actually construct the database (which is done in the
     * onCreate() hook method). It places the database in the
     * application's cache directory, which will be automatically
     * cleaned up by Android if the device runs low on storage space.
     *
     * @param context Any context
     */

    override fun onCreate(db: SQLiteDatabase) {
        Log.d("pantherman", "trying to create table")
        this.db = db
        this.db.execSQL(CREATE_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        this.db.execSQL("DROP TABLE IF EXISTS ${FavoriteContract.FavoriteEntry.TABLE_NAME}")
        onCreate(this.db)
    }
}