package org.central.app.sqliteSupport

import android.content.*
import android.database.Cursor
import android.net.Uri
import android.text.TextUtils
import android.util.Log
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteDatabase
import java.io.File

/**
 * very elaborate implementation of ContentProvider
 *
 * the point of a ContentProvider is to make it so other apps can interact with your database. Seems like it's more likely
 * that you would be trying to interact with some other app's content provider than creating one yourself, but oh well
 *
 * a content provider could be using some other resource that isn't sqlite, that's why this UriMatcher stuff is used.
 *
 * when using a content provider, you need to also use the activity's ContentResolver. It ends up having a more indirect feel
 * then manipulating a sqlite database created manually. If you weren't doing a content provider, you could just run raw queries inline
 * anywhere in the app. With this, you need to get the resolver from the activity and use these methods and this odd set of arguments.
 *
 * working with this is a good exercise, but ultimately, this app doesn't share the 'favorites' database with any other apps, therefor
 * it could just have an SQLiteOpenHelper and that's it
 */

class FavoritesProvider : ContentProvider() {

    protected val TAG = "sillyman"

    var mContext: Context? = null

    /**
     * Use FavoritesHelper to manage database creation and version
     * management.
     */
    private var mOpenHelper: FavoritesHelper? = null

    /**
     * Return true if successfully started.
     */
    override fun onCreate(): Boolean {
        mContext = context
        mOpenHelper = FavoritesHelper(context)
        val db = mOpenHelper?.writableDatabase
        return db != null
    }

    /**
     * The code that is returned when a URI for more than 1 items is
     * matched against the given components.  Must be positive.
     */
    val CHARACTERS = 100

    /**
     * The code that is returned when a URI for exactly 1 item is
     * matched against the given components.  Must be positive.
     */
    val CHARACTER = 101

    /**
     * The URI Matcher used by this content provider.
     */
    private val sUriMatcher = buildUriMatcher()

    /**
     * Helper method to match each URI to the ACRONYM integers
     * constant defined above.
     *
     * @return UriMatcher
     */
    private fun buildUriMatcher(): UriMatcher {
        // All paths added to the UriMatcher have a corresponding code
        // to return when a match is found.  The code passed into the
        // constructor represents the code to return for the rootURI.
        // It's common to use NO_MATCH as the code for this case.
        val matcher = UriMatcher(UriMatcher.NO_MATCH)

        // For each type of URI that is added, a corresponding code is
        // created.
        matcher.addURI(
            FavoriteContract.CONTENT_AUTHORITY,
            FavoriteContract.PATH_CHARACTER, CHARACTERS)
        matcher.addURI(FavoriteContract.CONTENT_AUTHORITY, FavoriteContract.PATH_CHARACTER + "/#", CHARACTER)
        return matcher
    }

    /**
     * Method called to handle type requests from client applications.
     * It returns the MIME type of the data associated with each
     * URI.
     */
    override fun getType(uri: Uri): String? {
        // Match the id returned by UriMatcher to return appropriate
        // MIME_TYPE.
        when (sUriMatcher.match(uri)) {
            CHARACTERS -> return FavoriteContract.FavoriteEntry.CONTENT_ITEMS_TYPE
            CHARACTER -> return FavoriteContract.FavoriteEntry.CONTENT_ITEM_TYPE
            else -> throw UnsupportedOperationException("Unknown uri: $uri")
        }
    }

    /**
     * Method called to handle insert requests from client apps.
     */
    override fun insert(uri: Uri, cvs: ContentValues?): Uri? {
        val returnUri: Uri

        printCharacters("inserting", cvs, uri)

        // Try to match against the path in a url.  It returns the
        // code for the matched node (added using addURI), or -1 if
        // there is no matched node.  If there's a match insert a new
        // row.
        when (sUriMatcher.match(uri)) {
            CHARACTERS -> returnUri = insertCharacters(uri, cvs)
            else -> throw UnsupportedOperationException("Unknown uri: $uri")
        }

        // Notifies registered observers that a row was inserted.
        mContext!!.contentResolver.notifyChange(uri, null)
        return returnUri
    }

    private fun insertCharacters(uri: Uri, cvs: ContentValues?): Uri {
        val db = mOpenHelper!!.writableDatabase

        val id = db.insert(FavoriteContract.FavoriteEntry.TABLE_NAME, null, cvs)

        // Check if a new row is inserted or not.
        return if (id > 0)
            FavoriteContract.FavoriteEntry.buildUri(id)
        else
            throw android.database.SQLException("Failed to insert row into $uri")
    }

    /**
     * Method that handles bulk insert requests.
     */
    override fun bulkInsert(uri: Uri, cvsArray: Array<ContentValues>): Int {

        for (cvs in cvsArray) printCharacters("bulk inserting", cvs, uri)

        // Try to match against the path in a url.  It returns the
        // code for the matched node (added using addURI), or -1 if
        // there is no matched node.  If there's a match bulk insert
        // new rows.
        when (sUriMatcher.match(uri)) {
            CHARACTERS -> {
                val returnCount = bulkInsertCharacters(uri, cvsArray)

                if (returnCount > 0)
                // Notifies registered observers that row(s) were
                // inserted.
                    mContext!!.contentResolver.notifyChange(uri, null)
                return returnCount
            }
            else -> throw UnsupportedOperationException()
        }
    }

    /**
     * Method that handles bulk insert requests.
     */
    private fun bulkInsertCharacters(uri: Uri, cvsArray: Array<ContentValues>): Int {
        // Create and/or open a database that will be used for reading
        // and writing. Once opened successfully, the database is
        // cached, so you can call this method every time you need to
        // write to the database.
        val db = mOpenHelper!!.writableDatabase

        var returnCount = 0

        // Begins a transaction in EXCLUSIVE mode.
        db.beginTransaction()
        try {
            for (cvs in cvsArray) {
                val id = db.insert(FavoriteContract.FavoriteEntry.TABLE_NAME, null, cvs).toInt()
                if (id != -1) returnCount++
            }

            // Marks the current transaction as successful.
            db.setTransactionSuccessful()
        } finally {
            // End a transaction.
            db.endTransaction()
        }
        return returnCount
    }

    /**
     * Method called to handle query requests from client
     * applications.
     */
    override fun query(uri: Uri, projection: Array<String>?, selection: String?, selectionArgs: Array<String>?, sortOrder: String?): Cursor? {
        val cursor: Cursor

        // Match the id returned by UriMatcher to query appropriate
        // rows.
        when (sUriMatcher.match(uri)) {
            CHARACTERS -> cursor = queryCharacters(uri, projection, selection, selectionArgs, sortOrder)
            CHARACTER -> cursor = queryCharacter(uri, projection, selection, selectionArgs, sortOrder)
            else -> throw UnsupportedOperationException("Unknown uri: $uri")
        }

        // Register to watch a content URI for changes.
        cursor.setNotificationUri(mContext!!.contentResolver, uri)
        return cursor
    }

    /**
     * Method called to handle query requests from client
     * applications.
     */
    private fun queryCharacters(uri: Uri, projection: Array<String>?, selection: String?, selectionArgs: Array<String>?, sortOrder: String?): Cursor {
        var selection = selection
        // Expand the selection if necessary.
        selection = addSelectionArgs(selection, selectionArgs, "OR")
        return mOpenHelper!!.readableDatabase.query(
            FavoriteContract.FavoriteEntry.TABLE_NAME,
            projection,
            selection,
            selectionArgs,
            null,
            null,
            sortOrder
        )
    }

    /**
     * Method called to handle query requests from client
     * applications.
     */
    private fun queryCharacter(uri: Uri, projection: Array<String>?, selection: String?, selectionArgs: Array<String>?, sortOrder: String?): Cursor {
        // Query the SQLite database for the particular rowId based on
        // (a subset of) the parameters passed into the method.
        return mOpenHelper!!.readableDatabase.query(
            FavoriteContract.FavoriteEntry.TABLE_NAME,
            projection,
            addKeyIdCheckToWhereStatement(selection, ContentUris.parseId(uri)),
            selectionArgs,
            null,
            null,
            sortOrder
        )
    }

    /**
     * Method called to handle update requests from client
     * applications.
     */
    override fun update(uri: Uri, cvs: ContentValues?, selection: String?, selectionArgs: Array<String>?): Int {
        val returnCount: Int

        printCharacters("updating", cvs, uri)

        // Try to match against the path in a url.  It returns the
        // code for the matched node (added using addURI), or -1 if
        // there is no matched node.  If there's a match update rows.
        when (sUriMatcher.match(uri)) {
            CHARACTERS -> returnCount = updateCharacters(uri, cvs, selection, selectionArgs)
            CHARACTER -> returnCount = updateCharacter(uri, cvs, selection, selectionArgs)
            else -> throw UnsupportedOperationException()
        }

        if (returnCount > 0)
        // Notifies registered observers that row(s) were
        // updated.
            mContext!!.contentResolver.notifyChange(uri, null)
        return returnCount
    }

    /**
     * Method called to handle update requests from client
     * applications.
     */
    private fun updateCharacters(uri: Uri, cvs: ContentValues?, selection: String?, selectionArgs: Array<String>?): Int {
        var selection = selection
        // Expand the selection if necessary.
        selection = addSelectionArgs(selection, selectionArgs, " OR ")
        return mOpenHelper!!.writableDatabase
            .update(FavoriteContract.FavoriteEntry.TABLE_NAME, cvs, selection, selectionArgs)
    }

    /**
     * Method called to handle update requests from client
     * applications.
     */
    private fun updateCharacter(uri: Uri, cvs: ContentValues?, selection: String?, selectionArgs: Array<String>?): Int {
        var selection = selection
        // Expand the selection if necessary.
        selection = addSelectionArgs(selection, selectionArgs, " OR ")
        // Just update a single row in the database.
        return mOpenHelper!!.writableDatabase.update(
            FavoriteContract.FavoriteEntry.TABLE_NAME,
            cvs,
            addKeyIdCheckToWhereStatement(selection, ContentUris.parseId(uri)),
            selectionArgs
        )
    }

    /**
     * Method called to handle delete requests from client
     * applications.
     */
    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<String>?): Int {
        val returnCount: Int

        printSelectionArgs("deleting", selection, selectionArgs, uri)

        // Try to match against the path in a url.  It returns the
        // code for the matched node (added using addURI), or -1 if
        // there is no matched node.  If there's a match delete rows.
        when (sUriMatcher.match(uri)) {
            CHARACTERS -> returnCount = deleteCharacters(uri, selection, selectionArgs)
            CHARACTER -> returnCount = deleteCharacter(uri, selection, selectionArgs)
            else -> throw UnsupportedOperationException()
        }

        if (selection == null || returnCount > 0)
        // Notifies registered observers that row(s) were deleted.
            mContext!!.contentResolver.notifyChange(uri, null)

        return returnCount
    }

    /**
     * Method called to handle delete requests from client
     * applications.
     */
    private fun deleteCharacters(uri: Uri, selection: String?, selectionArgs: Array<String>?): Int {
        var selection = selection
        // Expand the selection if necessary.
        selection = addSelectionArgs(selection, selectionArgs, " OR ")
        return mOpenHelper!!.writableDatabase
            .delete(FavoriteContract.FavoriteEntry.TABLE_NAME, selection, selectionArgs)
    }

    /**
     * Method called to handle delete requests from client
     * applications.
     */
    private fun deleteCharacter(uri: Uri, selection: String?, selectionArgs: Array<String>?): Int {
        var selection = selection
        // Expand the selection if necessary.
        selection = addSelectionArgs(selection, selectionArgs, " OR ")
        // Just delete a single row in the database.
        return mOpenHelper!!.writableDatabase.delete(
            FavoriteContract.FavoriteEntry.TABLE_NAME,
            addKeyIdCheckToWhereStatement(selection, ContentUris.parseId(uri)),
            selectionArgs
        )
    }

    /**
     * Return a selection string that concatenates all the
     * @a selectionArgs for a given @a selection using the given @a
     * operation.
     */
    private fun addSelectionArgs(selection: String?, selectionArgs: Array<String>?, operation: String): String? {
        // Handle the "null" case.
        if (selection == null || selectionArgs == null)
            return null
        else {
            var selectionResult = ""

            // Properly add the selection args to the selectionResult.
            for (i in 0 until selectionArgs.size - 1)
                selectionResult += "$selection = ? $operation "

            // Handle the final selection case.
            selectionResult += "$selection = ?"

            printSelectionArgs(operation, selectionResult, selectionArgs, null)
            return selectionResult
        }
    }

    /**
     * Helper method that appends a given key id to the end of the
     * WHERE statement parameter.
     */
    private fun addKeyIdCheckToWhereStatement(whereStatement: String?, id: Long): String {
        val newWhereStatement: String
        if (TextUtils.isEmpty(whereStatement))
            newWhereStatement = ""
        else
            newWhereStatement = whereStatement!! + " AND "

        // Append the key id to the end of the WHERE statement.
        return newWhereStatement + FavoriteContract.FavoriteEntry._ID + " = '" + id + "'"
    }

    /**
     * Print out the characters to logcat.
     *
     * @param operation
     * @param cvs
     * @param uri
     */
    fun printCharacters(operation: String, cvs: ContentValues?, uri: Uri) {
        Log.d(TAG, "$operation on poop $uri")
        for (key in cvs!!.keySet()) {
            Log.d(TAG, key + " " + cvs.get(key))
        }
    }

    /**
     * Printout the selection args to logcat.
     *
     * @param operation
     * @param selectionResult
     * @param selectionArgs
     */
    fun printSelectionArgs(operation: String, selectionResult: String?, selectionArgs: Array<String>?, uri: Uri?) {
        // Output the selectionResults to Logcat.
        Log.d(TAG, operation + " on " + (uri ?: "null") + " selection = " + selectionResult + " selectionArgs = ")
        if (selectionArgs != null && selectionArgs.isNotEmpty())
            for (args in selectionArgs)
                Log.d(TAG, "$args ")
    }
}
