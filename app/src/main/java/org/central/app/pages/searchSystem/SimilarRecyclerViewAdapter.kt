package org.central.app.pages.searchSystem

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.search_results_content.view.*
import org.central.app.R
import org.central.app.controllers.HttpController
import org.central.app.objects.SimilarItem

/**
 * this was derived from the 'master detail' android studio template. That one had 2 lists and had activities - this has one list
 * and has fragments instead
 */

class SimilarRecyclerViewAdapter(private val parentActivity: Similar, var values: MutableList<SimilarItem>) : RecyclerView.Adapter<SimilarRecyclerViewAdapter.ViewHolder>() {

    val http = HttpController()

    private val onClickListener = View.OnClickListener { v ->
        val item = v.tag as SimilarItem

        val bundle = Bundle()

        // these were bundled as intent extras in the original implementation - for fragments 'arguments' are used the same way
        bundle.putString("ITEM_NAME", item.name)
        bundle.putString("ITEM_W_TEASER", item.wTeaser.trim())
        bundle.putString("ITEM_Y_ID", item.yId)
        bundle.putString("ITEM_Y_URL", item.yUrl)
        bundle.putString("ITEM_W_URL", item.wUrl)

        val itemDetail = ItemDetail()
        itemDetail.arguments = bundle

        // instatiating the debugger so I can easily swap out the itemDetail page with it to see the raw json
        val itemDetailDebugger = SimilarDebugger()
        itemDetailDebugger.arguments = bundle

        parentActivity.fragmentManager!!.beginTransaction()
            .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
            .replace(R.id.parent_container, itemDetail) // I would put the debugger in here while developing
            .addToBackStack("ItemDetail") // this makes the back button bring you back to the list
            .commit()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.search_results_content, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.idView.text = item.name

        with(holder.itemView) {
            tag = item
            setOnClickListener(onClickListener)
        }
    }

    override fun getItemCount() = values.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val idView = view.id_text
    }
}