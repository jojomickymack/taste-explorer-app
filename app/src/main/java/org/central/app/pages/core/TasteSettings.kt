package org.central.app.pages.core

import android.app.AlertDialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import org.central.app.R
import android.content.ContentValues
import org.central.app.sqliteSupport.FavoriteContract
import org.central.app.ParentActivity
import android.content.Intent
import org.central.app.controllers.SqliteController

/**
 * this is the 'settings' page. Originally I used it for debugging the ContentProvider. Now it clears the sqlite database
 * and turns the music off and on using a broadcast intent
 */

class TasteSettings : Fragment() {

    lateinit var alert: AlertDialog

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val parent = activity as ParentActivity
        val sqlite = SqliteController(parent.contentResolver)

        // setting up the message that appears when you click the 'clear database' button
        val builder = AlertDialog.Builder(this.context)
        builder.setTitle("database")
            .setMessage("your favorites have been cleared")

        alert = builder.create()

        val rootView = inflater.inflate(R.layout.taste_settings_main, container, false)

        val clearFavorites = rootView.findViewById(R.id.settings_clear_favorites) as Button

        val startMusic = rootView.findViewById(R.id.settings_start_button) as Button
        val stopMusic = rootView.findViewById(R.id.settings_stop_button) as Button

        // call the SqliteController to clear the database, pop up the AlertDialog to confirm that's what happened
        clearFavorites.setOnClickListener { v ->
            sqlite.clearFavorites()
            alert.show()
        }

        startMusic.setOnClickListener { v ->
            val broadcastIntent = Intent()
            broadcastIntent.action = "org.central.app.Start" // this is the same string I put in the MusicPlayingService
            parent.sendBroadcast(broadcastIntent)
        }

        stopMusic.setOnClickListener { v ->
            val broadcastIntent = Intent()
            broadcastIntent.action = "org.central.app.Stop" // same thing from MusicPlayingService
            parent.sendBroadcast(broadcastIntent)
        }

        return rootView
    }
}