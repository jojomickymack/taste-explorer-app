package org.central.app.pages

import android.os.Bundle
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import org.central.app.R
import org.central.app.pages.welcomeTransitions.*

/**
 * the FragmentPagerAdapter assumes you want to swish Fragments back and forth with gestures - it's perfect for
 * the welcome screen. It's like a carousel of fragments - I think it had some tab thing at the top originally which
 * would tell you which page you're on - this doesn't even require that, so it's almost no code!
 */

class Welcome : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.welcome_main)
        val viewPager: ViewPager = findViewById(R.id.welcome_view_pager)

        val adapter = WelcomePagerAdapter(supportFragmentManager)
        adapter.fragmentList += listOf(Welcome1(), Welcome2(), Welcome3(), Welcome4(), Welcome5())
        viewPager.adapter = adapter
    }
}