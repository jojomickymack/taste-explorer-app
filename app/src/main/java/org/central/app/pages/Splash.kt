package org.central.app.pages

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button

import org.central.app.R
import org.central.app.pages.core.Favorites
import org.central.app.pages.core.Search
import org.central.app.pages.core.TasteSettings

/**
 * I was originally imagining that this 'Loading' page would see if there were any sqlite records, check if internet was available,
 * and test out the api key, but I ended up ditching all that and having it just serve as a navigation. I then moved the navigation
 * into the toolbar, so when this is shown it's pretty much 2 ways to get to 'search', 'favorites', and 'settings'.
 *
 * It shouldn't be called Loading - it doesn't load anything! It's pretty much a title screen and the old navigation
 */

class Splash : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.splash_main, container, false)
        val welcomeButton = rootView.findViewById(R.id.splash_welcome_button) as Button
        val settingsButton = rootView.findViewById(R.id.splash_settings_button) as Button
        val favoritesButton = rootView.findViewById(R.id.splash_favorites_button) as Button

        // nothing crazy here - you touch the button, it swaps out the current fragment for the one that is the page you wanted
        welcomeButton.setOnClickListener { v ->
            v.context.startActivity(Intent(v.context, Welcome::class.java))

            fragmentManager!!.beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out) // these fade in/out animations are really easy, they are in res/anim
                .replace(R.id.parent_container, Search())
                .addToBackStack("Loading") // this is necessary for the back button to work - you can just as well put null in there
                .commit()
        }

        settingsButton.setOnClickListener { v ->
            fragmentManager!!.beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .replace(R.id.parent_container, TasteSettings())
                .addToBackStack("Loading")
                .commit()
        }

        favoritesButton.setOnClickListener { v ->
            fragmentManager!!.beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .replace(R.id.parent_container, Favorites())
                .addToBackStack("Loading")
                .commit()
        }

        return rootView
    }
}

