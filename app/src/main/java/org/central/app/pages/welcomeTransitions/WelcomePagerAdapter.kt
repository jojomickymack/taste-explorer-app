package org.central.app.pages.welcomeTransitions

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.app.FragmentManager

/**
 * I found this example somewhere after getting confused with the android studio template for a 'tabbed activity'
 * that 'tabbed activity' is very annoying because it uses some weird 'LiveData' construct - it's kind of similar to
 * RecyclerView where you can update it on the fly, but I just couldn't figure it out
 *
 * this solution is really simple - FragmentPagerAdapter just has a getItem and getCount method, and a list of child fragments - easy
 */

class WelcomePagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    val fragmentList = mutableListOf<Fragment>()

    override fun getItem(position: Int): Fragment {
        return fragmentList[position]
    }

    override fun getCount(): Int {
        return fragmentList.size
    }
}