package org.central.app.pages.core

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.util.Log
import org.central.app.ParentActivity
import android.app.AlertDialog

import org.central.app.R
import org.central.app.controllers.SqliteController
import org.central.app.objects.Favorite
import org.central.app.sqliteSupport.FavoriteContract

/**
 * this fragment gets put into the ParentActivity's 'parent_container' area, which is a 'FrameLayout'
 * there's a toolbar at the top that says 'favorites', and a RecyclerView with a list of 'Favorites' that
 * contains data from the sqlite database. If the database doesn't have any favorites, an AlertDialog pops up
 * telling you that 'you don't have any favorites yet'
 */

class Favorites : Fragment() {

    lateinit var myView: View
    lateinit var itemList: RecyclerView
    var favoritesList = mutableListOf<Favorite>()
    lateinit var adapter: FavoritesRecyclerViewAdapter
    lateinit var alert: AlertDialog

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.favorites_main, container, false)

        // the contentResolver comes from the ParentActivity, and the SqliteController needs it to access the db
        val parent = activity as ParentActivity
        val sqlite = SqliteController(parent.contentResolver)

        // getting the AlertDialog ready
        val builder = AlertDialog.Builder(this.context)
        builder.setTitle("empty database")
            .setMessage("you don't have any favorites yet")

        alert = builder.create()

        // this list is empty if there's no records in the 'favorites' table
        favoritesList = sqlite.getFavorites()

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        myView = view

        // this is the exact same pattern used in the SearchResults and ItemDetail RecyclerViews
        itemList = view.findViewById(R.id.item_list) as RecyclerView
        adapter = FavoritesRecyclerViewAdapter(this, favoritesList)
        itemList.adapter = adapter

        // here's where the message pops up telling you the database is empty
        if (favoritesList.isEmpty()) alert.show()
    }
}