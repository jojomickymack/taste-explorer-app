package org.central.app.pages.searchSystem

import android.os.AsyncTask
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.app.AlertDialog
import android.app.Dialog

import org.central.app.R
import org.central.app.controllers.HttpController
import org.central.app.controllers.JsonController
import org.central.app.objects.ItemAutocomplete

/**
 * this is just like the ItemDetail and Favorites pages - there's a Recycler list, and when this page is loaded with the
 * autocomplete endpoint request as an argument, this shows a spinner while the http request occurs, updates the list of items
 * and notifies the list adapter that the item list has changed
 */

class SearchResults : Fragment() {

    lateinit var myView: View
    lateinit var resultsFragment: SearchResults
    lateinit var itemList: RecyclerView
    var searchResultsList = mutableListOf<ItemAutocomplete>()
    lateinit var adapter: SearchResultsRecyclerViewAdapter
    private var autocompleteUrl: String? = ""

    private lateinit var dialog: Dialog

    /**
     * just like the others, this AsyncTask shows a Dialog that says 'loading', makes the http request to tastedive,
     * then parses the json response, updates the RecyclerView's list and removes the dialog
     * the reason this is here and is an inner class is because it has to have the ui elements it's updating in scope.
     *
     * seems like a fine solution to have it in this file - it's not called from any other view obviously
     */

    inner class GetSearchResultsUpdateUi : AsyncTask<String, Void, String>() {

        private val http = HttpController()
        private val json = JsonController()

        override fun onPreExecute() {
            dialog.show()
        }

        override fun doInBackground(vararg requestUrl: String?): String? {
            return http.doGetRequest(requestUrl[0])
        }

        override fun onPostExecute(jsonString: String?) {
            dialog.dismiss()

            // parse the json response into a list of ItemAutocomplete objects and update the list adaptor, then notify it of the change
            searchResultsList = json.parseAutocomplete(jsonString.toString())
            adapter.values = searchResultsList
            itemList.adapter?.notifyDataSetChanged()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.search_results_main, container, false)

        // set up my 'spinner' which shows that the http request is happening
        val builder = AlertDialog.Builder(context)
        builder.setCancelable(false)
        builder.setView(R.layout.spinner)
        dialog = builder.create()

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // the url to make the request to was sent as an argument when the old fragment was replaced
        if (arguments!!.containsKey("AUTOCOMPLETE_URL")) autocompleteUrl = arguments!!.getString("AUTOCOMPLETE_URL")

        GetSearchResultsUpdateUi().execute(autocompleteUrl)

        myView = view
        resultsFragment = this

        // the RecyclerView gets created with an empty list at first - think about it - it's part of the view and
        // needs to be on the screen onViewCreated, but the http request has to happen first - the ui can't be blocked
        // by long running actions like http requests - so it's best to create it with the empty list, then change it later
        itemList = view.findViewById(R.id.search_results_item_list) as RecyclerView
        adapter = SearchResultsRecyclerViewAdapter(this, searchResultsList)
        itemList.adapter = adapter
    }
}