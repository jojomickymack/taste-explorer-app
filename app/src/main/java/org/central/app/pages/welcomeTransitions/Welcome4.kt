package org.central.app.pages.welcomeTransitions

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.central.app.R

/**
 * nothing special about the fragment, it's just for loading a unique view. It literally just gets swiped off 2 seconds after it's shown
 */

class Welcome4 : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.welcome4_textview, container, false)
    }
}