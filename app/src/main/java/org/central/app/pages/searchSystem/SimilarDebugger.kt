package org.central.app.pages.searchSystem

import android.os.AsyncTask
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser

import org.central.app.R
import org.central.app.controllers.HttpController
import org.central.app.controllers.JsonController

/**
 * this just renders the tastedive response in a TextView so I can see what it looks like
 */

class SimilarDebugger : Fragment() {
    private val http = HttpController()
    private val json = JsonController()
    private val parser = Parser.default()
    private var similarUrl: String? = ""
    private lateinit var debugText: TextView

    /**
     * same AsyncTask structure as the real ItemDetail, just updates something else
     */

    inner class GetSearchResultsUpdateUi : AsyncTask<String, Void, String>() {

        override fun doInBackground(vararg requestUrl: String?): String? {
            return http.doGetRequest(requestUrl[0])
        }

        override fun onPostExecute(jsonString: String?) {
            val itemList = json.parseSimilar(jsonString.toString())
            Log.d("alakazam", itemList.size.toString())

            val parsedJson = parser.parse(StringBuilder(jsonString.toString())) as JsonObject
            debugText.text = parsedJson.toJsonString(true)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.debug_main, container, false)

        if (arguments!!.containsKey("SIMILAR_URL")) similarUrl = arguments!!.getString("SIMILAR_URL")

        debugText = rootView.findViewById(R.id.debug_text) as TextView

        // trigger the aync task
        GetSearchResultsUpdateUi().execute(similarUrl)
        return rootView
    }
}
