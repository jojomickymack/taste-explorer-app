package org.central.app.pages.searchSystem

import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.support.design.widget.CollapsingToolbarLayout
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.TextView
import org.central.app.ParentActivity

import org.central.app.R
import org.central.app.controllers.HttpController
import org.central.app.controllers.SqliteController
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL


class ItemDetail : Fragment() {

    val http = HttpController()
    private var itemName: String? = ""
    private var wTeaser: String? = ""
    private var yId: String? = ""
    private var wUrl: String? = ""
    private var yUrl: String? = ""
    lateinit var titleToolbar: CollapsingToolbarLayout
    private lateinit var alert: AlertDialog

    inner class DownloadImageUpdateToolbar : AsyncTask<String, Void, Bitmap>() {

        override fun doInBackground(vararg path: String): Bitmap? {
            var inputSteam: InputStream?
            var bitmap: Bitmap
            var responseCode: Int

            val url = URL(path[0])
            val con = url.openConnection() as HttpURLConnection
            con.doInput = true
            con.connect()
            responseCode = con.responseCode
            if (responseCode == HttpURLConnection.HTTP_OK) {
                inputSteam = con.inputStream
                bitmap = BitmapFactory.decodeStream(inputSteam)
                inputSteam.close()
            } else {
                bitmap = BitmapFactory.decodeResource(resources, R.drawable.error)
            }
            return bitmap
        }

        override fun onPostExecute(bitmap: Bitmap?) {
            titleToolbar.background = BitmapDrawable(resources, bitmap)
            super.onPostExecute(bitmap)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.item_detail_main, container, false)
        titleToolbar = rootView.findViewById(R.id.item_detail_toolbar_layout) as CollapsingToolbarLayout
        val teaserTextview = rootView.findViewById(R.id.item_detail_teaser_text) as TextView

        val wikipedia = rootView.findViewById(R.id.item_detail_toolbar_wikipedia) as ImageButton
        val youtube = rootView.findViewById(R.id.item_detail_toolbar_youtube) as ImageButton
        val search = rootView.findViewById(R.id.item_detail_toolbar_search) as ImageButton
        val favorites = rootView.findViewById(R.id.item_detail_toolbar_favorites) as ImageButton

        val builder = AlertDialog.Builder(this.context)
        builder.setTitle("database")
            .setMessage("the item has been saved to the database")
        alert = builder.create()

        val parent = activity as ParentActivity
        val sqlite = SqliteController(parent.contentResolver)

        // val okButton = rootView.findViewById(R.id.search_submit_button) as Button

        if (arguments!!.containsKey("ITEM_NAME")) itemName = arguments!!.getString("ITEM_NAME")
        if (arguments!!.containsKey("ITEM_W_TEASER")) wTeaser = arguments!!.getString("ITEM_W_TEASER")
        if (arguments!!.containsKey("ITEM_Y_ID")) yId = arguments!!.getString("ITEM_Y_ID")
        if (arguments!!.containsKey("ITEM_Y_URL")) yUrl = arguments!!.getString("ITEM_Y_URL")
        if (arguments!!.containsKey("ITEM_W_URL")) wUrl = arguments!!.getString("ITEM_W_URL")

        titleToolbar.background = ContextCompat.getDrawable(this.context!!, R.drawable.error)
        titleToolbar.title = itemName
        teaserTextview.text = wTeaser

        if (yUrl!!.indexOf("null") == -1) {
            Log.d("downtownclown", yUrl)
            youtube.setOnClickListener { v ->
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:$yId")))
            }
        } else youtube.visibility = View.GONE

        if (wUrl!!.indexOf("null") == -1) {
            Log.d("downtownclown", wUrl)
            wikipedia.setOnClickListener { v ->
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(wUrl)))
            }
        } else wikipedia.visibility = View.GONE

        search.setOnClickListener { v ->
            val bundle = Bundle()

            val similarUrl = http.getSimilarUrlFromName(itemName.toString())

            // these were bundled as intent extras in the original implementation - for fragments 'arguments' are used the same way
            bundle.putString("SIMILAR_URL", similarUrl)
            bundle.putString("ITEM_NAME", itemName.toString())
            val similar = Similar()
            similar.arguments = bundle

            // instantiating the debugger so I can easily swap out the 'similar' page with it to see the raw json
            val similarDebugger = SimilarDebugger()
            similarDebugger.arguments = bundle

            fragmentManager!!.beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .replace(R.id.parent_container, similar) // I would put the debugger in here while developing
                .addToBackStack("ItemDetail") // this makes the back button bring you back to the list
                .commit()
        }

        // you liked the item, insert it to database and show the notification that it happened
        favorites.setOnClickListener { v ->
            sqlite.insertFavorite(itemName.toString(), wTeaser.toString(), yId.toString(), wUrl.toString(), yUrl.toString(), "unused field")
            alert.show()
        }

        if (yId!!.isNotEmpty()) triggerImageRetrieval(http.getYoutubeImageUrl(yId.toString()))

        return rootView
    }

    // you can't block the ui, trigger this async task and move on. I probably shouldn't even do the task if the yId is blank, oh well
    private fun triggerImageRetrieval(yId: String) {
        DownloadImageUpdateToolbar().execute(yId)
    }
}
