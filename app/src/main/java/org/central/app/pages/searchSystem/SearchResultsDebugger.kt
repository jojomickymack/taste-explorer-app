package org.central.app.pages.searchSystem

import android.os.AsyncTask
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser

import org.central.app.R
import org.central.app.controllers.HttpController

/**
 * this is just like the other debugger - does the http request to tastedive and just puts the raw json in a TextView instead
 * of parsing it
 */

class SearchResultsDebugger : Fragment() {

    private val http = HttpController()
    private val parser = Parser.default()
    private var autocompleteUrl: String? = ""
    private lateinit var debugText: TextView

    // just like the others - update the view once the http request is finished - do not block the ui
    inner class GetSearchResultsUpdateUi : AsyncTask<String, Void, String>() {

        override fun doInBackground(vararg requestUrl: String?): String? {
            return http.doGetRequest(requestUrl[0])
        }

        override fun onPostExecute(jsonString: String?) {
            val parsedJson = parser.parse(StringBuilder(jsonString.toString())) as JsonObject
            debugText.text = parsedJson.toJsonString(true)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.debug_main, container, false)

        // get the url to make the request with - it was sent as an argument when the fragment was replaced
        if (arguments!!.containsKey("AUTOCOMPLETE_URL")) autocompleteUrl = arguments!!.getString("AUTOCOMPLETE_URL")

        debugText = rootView.findViewById(R.id.debug_text) as TextView

        // trigger the async tasks and return
        GetSearchResultsUpdateUi().execute(autocompleteUrl)
        return rootView
    }
}
