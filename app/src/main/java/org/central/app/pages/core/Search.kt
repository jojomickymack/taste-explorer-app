package org.central.app.pages.core

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText

import org.central.app.R
import org.central.app.controllers.HttpController
import android.content.Context.INPUT_METHOD_SERVICE
import android.view.inputmethod.InputMethodManager
import org.central.app.pages.searchSystem.SearchResults
import org.central.app.pages.searchSystem.SearchResultsDebugger


/**
 * here's where you put in your search term using an EditText
 */

class Search : Fragment() {

    val http = HttpController()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.search_main, container, false)
        val okButton = rootView.findViewById(R.id.search_submit_button) as Button
        val editText = rootView.findViewById(R.id.search_edit_text) as EditText

        okButton.setOnClickListener { v ->

            val imm = activity!!.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view!!.windowToken, 0)

            val bundle = Bundle()
            // get the text that the user entered, get a url for autocomplete results and send it as an argument
            // to the SearchResults page
            val query = editText.text.toString()

            val jsonString = http.getAutocompleteUrl(query)
            bundle.putString("AUTOCOMPLETE_URL", jsonString)

            val searchResults = SearchResults()
            // again - instantiate a debugger for the SearchResults so I can see the raw json
            val debugger = SearchResultsDebugger()
            debugger.arguments = bundle
            searchResults.arguments = bundle

            fragmentManager!!.beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .replace(R.id.parent_container, searchResults) // I'd just put the debugger object in here when developing
                .addToBackStack("SearchFragment") // you need this for the back button to bring you back here
                .commit()
        }

        return rootView
    }
}
