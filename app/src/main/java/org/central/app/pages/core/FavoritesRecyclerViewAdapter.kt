package org.central.app.pages.core

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.search_results_content.view.*
import org.central.app.R
import org.central.app.controllers.HttpController
import org.central.app.objects.Favorite
import org.central.app.pages.searchSystem.ItemDetail
import org.central.app.pages.searchSystem.Similar
import org.central.app.pages.searchSystem.SimilarDebugger

/**
 * the exact same pattern is used for SearchResults and ItemDetail views, the difference is what data is available
 * and what happens when you touch one of the listings. This was refactored from the original 'master detail' template in
 * android studio, the main difference being that this is done with Fragments and that one had Activities
 */

class FavoritesRecyclerViewAdapter(private val parentActivity: Favorites, var values: MutableList<Favorite>) : RecyclerView.Adapter<FavoritesRecyclerViewAdapter.ViewHolder>() {

    val http = HttpController()

    private val onClickListener = View.OnClickListener { v ->
        val favorite = v.tag as Favorite

        val bundle = Bundle()

        // bundle up the url and the name of the selected item, they are used to launch the http request to tastedive and show the
        // title in the toolbar - in the original example, these were sent with the intent extras - for Fragments, arguments
        // do the same thing
        bundle.putString("ITEM_NAME", favorite.name)
        bundle.putString("ITEM_W_TEASER", favorite.wTeaser)
        bundle.putString("ITEM_Y_ID", favorite.yId)
        bundle.putString("ITEM_Y_URL", favorite.yUrl)
        bundle.putString("ITEM_W_URL", favorite.wUrl)

        val itemDetail = ItemDetail()
        itemDetail.arguments = bundle

        // this debugger thing just shows the raw json from tastedive in a TextView
        val itemDetailDebugger = SimilarDebugger()
        itemDetailDebugger.arguments = bundle

        parentActivity.fragmentManager!!.beginTransaction()
            .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
            .replace(R.id.parent_container, itemDetail) // I would put 'debugger' in here to view the json while I was developing
            .addToBackStack("Favorites") // this line makes it so the back button works
            .commit()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.search_results_content, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.idView.text = item.name

        with(holder.itemView) {
            tag = item
            setOnClickListener(onClickListener)
        }
    }

    override fun getItemCount() = values.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val idView = view.id_text
    }
}