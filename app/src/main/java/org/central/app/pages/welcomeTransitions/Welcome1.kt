package org.central.app.pages.welcomeTransitions

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.central.app.R

/**
 * all this does is load the view associated with the class - the reason it exists is so each Fragmeent could be completely autonomous
 *
 * welcome 1 says 'welcome' and explains some things about the app
 */

class Welcome1 : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.welcome1_textview, container, false)
    }
}