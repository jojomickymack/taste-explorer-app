package org.central.app.pages.welcomeTransitions

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import org.central.app.R

/**
 * this fragment is more elaborate that welcome 1 and 2 because it asks you if you want to create an account on tastedive.com
 *
 * it runs an intent that requests to open the tastedive signin page using the default browser
 */

class Welcome5 : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.welcome5_textview, container, false)

        val useDefaultButton = view.findViewById(R.id.welcome5_default_button) as Button
        val signupButton = view.findViewById(R.id.welcome5_signup_button) as Button

        // this is a hack - since this 'Welcome' thing is an activity, the old 'ParentActivity' is still running beneath it
        // originally, I tried to manipulate the ParentActivity from here, but it's not easy. I just left the ParentActivity with
        // the fragment I want waiting while all this welcome stuff is happening, then just end the Welcome activity

        useDefaultButton.setOnClickListener { v ->
            activity?.finish()
        }

        // create an intent that opens the browser to the signin page when you click the 'signup' button

        signupButton.setOnClickListener { v ->
            val ft = fragmentManager!!.beginTransaction()
            ft.remove(this)
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .replace(R.id.welcome_frame, ApiKeyEntry(), "ApiKeyEntry")
                .commit()
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://tastedive.com/account/signin")))
        }

        return view
    }
}