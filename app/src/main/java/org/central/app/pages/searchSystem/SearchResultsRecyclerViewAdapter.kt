package org.central.app.pages.searchSystem

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.search_results_content.view.*
import org.central.app.R
import org.central.app.controllers.HttpController
import org.central.app.objects.ItemAutocomplete

/**
 * just like the Favorites and ItemDetail ViewAdaptors - supports the RecyclerView by handing the clicks and building this list
 *
 * again - this came from the original 'master - detail' template. That one had 2 lists and used activities. This one has one
 * list and is using Fragments
 */

class SearchResultsRecyclerViewAdapter(private val parentActivity: SearchResults, var values: MutableList<ItemAutocomplete>) : RecyclerView.Adapter<SearchResultsRecyclerViewAdapter.ViewHolder>() {

    val http = HttpController()

    private val onClickListener = View.OnClickListener { v ->
        val itemAutoComplete = v.tag as ItemAutocomplete

        val bundle = Bundle()

        // get the url so it can be sent to the ItemDetail fragment and used after it's on screen
        val name = http.getNameFromAutocomplete(itemAutoComplete)
        val similarUrl = http.getSimilarUrlFromName(name)

        bundle.putString("SIMILAR_URL", similarUrl)
        bundle.putString("ITEM_NAME", name)
        val itemDetail = Similar()
        itemDetail.arguments = bundle

        // just like the other ones, I have a debugger that lets me see the raw json on the screen
        val itemDetailDebugger = SimilarDebugger()
        itemDetailDebugger.arguments = bundle

        parentActivity.fragmentManager!!.beginTransaction()
            .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
            .replace(R.id.parent_container, itemDetail) // I would put 'debugger' here during development
            .addToBackStack("SearchResults")
            .commit()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.search_results_content, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.idView.text = item.value

        with(holder.itemView) {
            tag = item
            setOnClickListener(onClickListener)
        }
    }

    override fun getItemCount() = values.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val idView = view.id_text
    }
}