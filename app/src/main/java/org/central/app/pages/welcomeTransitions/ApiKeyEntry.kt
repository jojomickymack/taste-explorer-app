package org.central.app.pages.welcomeTransitions

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import org.central.app.R

/**
 * ok, this doesn't actually do anything - when I came up with the idea for the project, I thought the api had more functionality
 * I thought you'd be interacting with your own account and the 'favorites' were online and you'd be able to see a list of them
 * using the api. The idea with saving your key here was so you didn't see my favorites and would be able to build your own network
 *
 * the api doesn't support any of that - you can only get lists of recommendations from items you do http requests for - the api key
 * in use doesn't change anything.
 *
 * Still, this stuff was a good exercise and from the front end you don't know that it's not doing anything
 */

class ApiKeyEntry : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.welcome_apikey_entry, container, false)

        val submitButton = view.findViewById(R.id.welcome_apikey_submit_button) as Button
        val cancelButton = view.findViewById(R.id.welcome_apikey_cancel_button) as Button

        submitButton.setOnClickListener { v ->
            activity?.finish()
        }

        cancelButton.setOnClickListener { v ->
            activity?.finish()
        }

        return view
    }
}