package org.central.app.pages.searchSystem

import android.app.AlertDialog
import android.app.Dialog
import android.os.AsyncTask
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.support.v7.widget.Toolbar
import org.central.app.R
import org.central.app.controllers.HttpController
import org.central.app.controllers.JsonController
import org.central.app.objects.SimilarItem

/**
 * I was originally imagining a page where the wikipedia teaser would appear, but since there's really not enough space
 * and the names of the items looks good enough, that's how I left it. Really not much detail, but oh well.
 *
 * Putting the AsyncTasks in here is a solution to a problem I'd have if they were in a different file, I'd have to somehow
 * send the ui elements to the other class.
 *
 * When the AsyncTasks are inner classes to the Fragment they need to update, it's a lot easier!
 */

class Similar : Fragment() {

    lateinit var myView: View
    lateinit var similarFragment: Similar
    lateinit var itemList: RecyclerView
    var itemDetailList = mutableListOf<SimilarItem>()
    lateinit var adapter: SimilarRecyclerViewAdapter
    private var similarUrl: String? = ""
    private var itemName: String? = ""
    private lateinit var titleToolbar: Toolbar
    private lateinit var dialog: Dialog

    /**
     * this AsnycTask puts up a dialog that says 'loading' as a onPreExecute task, then dismisses it as a onPostExecute
     */

    inner class GetItemDetailUpdateUi : AsyncTask<String, Void, String>() {

        private val http = HttpController()
        private val json = JsonController()

        override fun onPreExecute() {
            dialog.show()
        }

        override fun doInBackground(vararg requestUrl: String?): String? {
            return http.doGetRequest(requestUrl[0])
        }

        override fun onPostExecute(jsonString: String?) {
            dialog.dismiss()

            // turn the json response from tastedive into a list of Items
            itemDetailList = json.parseSimilar(jsonString.toString())
            // update the RecyclerList's values (an empty list when the page first loads), then notify that it's changed
            adapter.values = itemDetailList
            itemList.adapter?.notifyDataSetChanged()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.similar_main, container, false)
        titleToolbar = rootView.findViewById(R.id.similar_toolbar) as Toolbar

        // build the Dialog that says 'loading' whild the http request occurs
        var builder = AlertDialog.Builder(this.context)
        builder.setCancelable(false)
        builder.setView(R.layout.spinner)
        dialog = builder.create()

        titleToolbar.title = "X"

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // get the 'arguments' sent when the 'onClickHandler' was set up
        if (arguments!!.containsKey("SIMILAR_URL")) similarUrl = arguments!!.getString("SIMILAR_URL")
        if (arguments!!.containsKey("ITEM_NAME")) itemName = arguments!!.getString("ITEM_NAME")

        // trigger the first AsyncTask
        GetItemDetailUpdateUi().execute(similarUrl)

        titleToolbar.title = "items similar to $itemName"

        myView = view
        similarFragment = this

        // just like SearchResults and Favorites, the ViewAdapter makes the RecyclerView work
        itemList = view.findViewById(R.id.similar_item_list) as RecyclerView
        adapter = SimilarRecyclerViewAdapter(this, itemDetailList)
        itemList.adapter = adapter
    }
}