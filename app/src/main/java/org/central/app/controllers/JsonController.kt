package org.central.app.controllers

import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import org.central.app.objects.SimilarItem
import org.central.app.objects.ItemAutocomplete

/**
 * json is returned from the tastedive controller, and it needs to be parsed out into lists of
 * either 'ItemAutocomplete' or 'Item' objects
 */

class JsonController {

    /**
     * klaxon is a kotlin library for handing json
     * https://github.com/cbeust/klaxon
     *
     * the parser object can pull java typed values out of a json object
     */

    val parser = Parser.default()

    /**
     * the autocomplete endpoint returns lists of objects having a 'value' and 'data' field
     * example
     * https://tastedive.com/api/autocomplete?v=3&t=&target=search&q=notorious
     *
     * I need to return a list of objects so my SearchResultsRecyclerViewAdaptor can use it to render the scrollable list
     */

    fun parseAutocomplete(json: String): MutableList<ItemAutocomplete> {
        val parsedJson = parser.parse(StringBuilder(json)) as JsonObject
        val suggestions = parsedJson.array<JsonObject>("suggestions")

        val suggestionList = mutableListOf<ItemAutocomplete>()

        for (i in 0 until suggestions!!.size) {
            val obj = suggestions[i]
            suggestionList += ItemAutocomplete(
                i,
                obj.string("value").toString(),
                obj.string("data").toString()
            )
        }

        return suggestionList
    }

    /**
     * the similar items endpoint returns much more data, and more still if you include the &info=1
     * these values will resolve to emptyString (which is what null resolves to when toString() is called on it)
     * if the field isn't in the json response, which is fine. The yID in particular gets used to retrieve an image
     * thumbnail if it's included - if not the default 404 image will be displayed
     */

    fun parseSimilar(json: String): MutableList<SimilarItem> {
        val parsedJson = parser.parse(StringBuilder(json)) as JsonObject
        val info = parsedJson.obj("Similar")?.array<JsonObject>("Info")

        val item = info!![0]

        val similarList = mutableListOf<SimilarItem>()
        val searchedForItem = SimilarItem(
            0,
            item.string("Name").toString(),
            item.string("Type").toString(),
            item.string("wTeaser").toString(),
            item.string("wUrl").toString(),
            item.string("yUrl").toString(),
            item.string("yID").toString()
        )
        similarList += searchedForItem // probably don't need the original item in the list

        val results = parsedJson.obj("Similar")?.array<JsonObject>("Results")

        for (i in 0 until results!!.size) {
            val obj = results[i]
            similarList += SimilarItem(
                i + 1,
                obj.string("Name").toString(),
                obj.string("Type").toString(),
                obj.string("wTeaser").toString(),
                obj.string("wUrl").toString(),
                obj.string("yUrl").toString(),
                obj.string("yID").toString()
            )
        }

        return similarList
    }
}