package org.central.app.controllers

import android.content.ContentResolver
import android.content.ContentValues
import org.central.app.objects.Favorite
import org.central.app.sqliteSupport.FavoriteContract

/**
 * this is basically aggregating the interactions with the sqlite ContentProvider so they aren't scattered around in the views
 * the ContentResolver instance (which lives in the ParentActivity) is used in the constructor
 */

class SqliteController(val resolver: ContentResolver) {

    /**
     * this is called from the settings page if you click the button to 'clear favorites'
     */

    fun clearFavorites() {
        resolver.delete(FavoriteContract.FavoriteEntry.CONTENT_URI, FavoriteContract.FavoriteEntry.NAME, null)
    }

    /**
     * this gets run when you tap the 'like' button on the item detail page
     */

    fun insertFavorite(name: String, wTeaser: String, yId: String, wUrl: String, yUrl: String, description: String) {
        val favoritesList = getFavorites()

        if (name !in favoritesList.groupBy { it.name }) {
            val values = ContentValues()
            values.put(FavoriteContract.FavoriteEntry.NAME, name)
            values.put(FavoriteContract.FavoriteEntry.W_TEASER, wTeaser)
            values.put(FavoriteContract.FavoriteEntry.Y_ID, yId)
            values.put(FavoriteContract.FavoriteEntry.W_URL, wUrl)
            values.put(FavoriteContract.FavoriteEntry.Y_URL, yUrl)
            values.put(FavoriteContract.FavoriteEntry.DESCRIPTION, description)
            resolver.insert(FavoriteContract.FavoriteEntry.CONTENT_URI, values)
        }
    }

    /**
     * returns the list of favorites to fill up the 'favorites' page. If there are not favorites it just returns an empty list
     */

    fun getFavorites(): MutableList<Favorite> {
        val favoritesList = mutableListOf<Favorite>()

        val cursor = resolver.query(FavoriteContract.FavoriteEntry.CONTENT_URI, FavoriteContract.FavoriteEntry.sColumnsToDisplay, FavoriteContract.FavoriteEntry.DESCRIPTION, null, null)

        if (cursor?.moveToFirst()!!) {
            do {
                favoritesList.add(Favorite(cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6)))
            } while (cursor.moveToNext())
        }
        cursor.close()

        return favoritesList
    }
}