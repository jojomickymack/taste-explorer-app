package org.central.app.controllers

import android.util.Log
import org.central.app.objects.ItemAutocomplete
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

/**
 * the strategy here is to put all the dealings with http in this controller, which is instantiated in various
 * views, it's a way to organize the code by its purpose.
 * This includes code that interacts with tastedive.com and youtube (to get thumbnail images)
 */

class HttpController {

    /**
     * getting the urls and actually doing the requests are done in completely different places -
     * urls are built on the Search (to the autocomplete endpoint), the SearchResults (to the similar endpoint),
     * ItemDetail (to the similar and the youtube image endpoints), and Favorites (to the similar endpoint)
     *
     * the http requests all occur inside of AsyncTasks inside of the view which displays data parsed
     * from the responses
     */

    fun doGetRequest(requestUrl: String?): String {
        val result = StringBuilder()
        val url = URL(requestUrl)
        val conn = url.openConnection() as HttpURLConnection
        conn.requestMethod = "GET"
        conn.setRequestProperty("Accept", "application/json")
        val br = BufferedReader(InputStreamReader(conn.inputStream))
        for (line in br.readLines()) {
            result.append(line)
        }
        br.close()
        return result.toString()
    }

    val key = "303248-coolapp0-7N4QE56Z"

    /**
     * tastedive returns the 'name' in the autocomplete's 'value' field but with the type in parenthesis.
     * {
     *   "value": "The Hitchhiker's Guide To The Galaxy (Book)",
     *   "data": "The-Hitchhiker-s-Guide-To-The-Galaxy-Book"
     * }
     * requires q=The+Hitchhiker's+Guide+To+The+Galaxy
     *
     * {
     *   "value": "The Notorious B.I.G. (Music)",
     *   "data": "The-Notorious-B-I-G"
     * }
     * requires q=The+Notorious+B.I.G.
     *
     * I just chop off everything after ' ('
     */

    fun getNameFromAutocomplete(itemAutocomplete: ItemAutocomplete): String {
        return if (itemAutocomplete.value.indexOf(" (") == -1) itemAutocomplete.value
        else itemAutocomplete.value.substring(0, itemAutocomplete.value.lastIndexOf(" ("))
    }

    /**
     * This is used from SearchResults, ItemDetailView, and Favorites - replaces spaces with + and appends it to the similar endpoint
     */

    fun getSimilarUrlFromName(name: String): String {
        val sb = StringBuilder()

        for(char in name){
            if (char == ' ') {
                sb.append('+')
            } else {
                sb.append(char)
            }
        }

        val query = sb.toString()
        val requestUrl = "https://tastedive.com/api/similar?k=$key&info=1&q=$query"
        Log.d("tasteExplorer", requestUrl)
        return requestUrl
    }

    /**
     * this is the request that gets done on the 'search' page - the endpoint is for autocompletion and returns valid 'items'
     * which can be submitted for 'similar items'
     */

    fun getAutocompleteUrl(query: String): String {
        val cleanQuery = query.replace(" ", "+")
        val requestUrl = "https://tastedive.com/api/autocomplete?k=$key&v=3&t=&target=search&q=$cleanQuery"
        Log.d("tasteExplorer", requestUrl)
        return requestUrl
    }

    /**
     * for many items in tastedive, there's a yId and youtubeUrl returned - youtube will return image thumbnails from the yId
     * 0.jpg is the highest resolution
     */

    fun getYoutubeImageUrl(yId: String): String {
        return "https://img.youtube.com/vi/$yId/0.jpg"
    }
}