package org.central.app.objects

/**
 * pojo representing a record in the 'favorites' table. Only the name is actually used, I was thinking of storing
 * the type and the wikipedia snippet, never did it
 */

data class Favorite(val name: String, val wTeaser: String, val yId: String, val wUrl: String, val yUrl: String, val description: String)