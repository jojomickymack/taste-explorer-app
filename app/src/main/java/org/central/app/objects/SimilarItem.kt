package org.central.app.objects

/**
 * this pojo represents a 'similar item' from the tastedive similar endpoint. Some of these fields may not be in the json and
 * will just be an empty string in that case
 */

data class SimilarItem(val id: Int, val name: String, val type: String, val wTeaser: String, val wUrl: String, val yUrl: String, val yId: String)