package org.central.app.objects

/**
 * the tastedive autocomplete endpoint doesn't return very much data, just the name in 2 different fields
 */

data class ItemAutocomplete(val id: Int, val value: String, val data: String)