package org.central.app

import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import org.central.app.controllers.SqliteController
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

/**
 * these are the tests that run with an actual connected device or emulated device
 */

@RunWith(AndroidJUnit4::class)
class SqliteControllerTests {

    val appContext = InstrumentationRegistry.getTargetContext()
    val sqlite = SqliteController(appContext.contentResolver)

    @Test
    fun clear_database_check_size() {
        sqlite.clearFavorites()
        val favoritesList = sqlite.getFavorites()
        Assert.assertEquals(0, favoritesList.size)
    }

    @Test
    fun clear_database_add_favorite_check_size() {
        sqlite.clearFavorites()
        sqlite.insertFavorite("Biggie", "unused field")
        val favoritesList = sqlite.getFavorites()
        Assert.assertEquals(1, favoritesList.size)
    }

    @Test
    fun clear_database_add_many_favorites_check_size() {
        sqlite.clearFavorites()
        sqlite.insertFavorite("Biggie", "unused field")
        sqlite.insertFavorite("Woogie", "unused field")
        sqlite.insertFavorite("PooBoyz", "unused field")
        sqlite.insertFavorite("Rap Doggie", "unused field")
        sqlite.insertFavorite("Sniffer Doodle", "unused field")
        val favoritesList = sqlite.getFavorites()
        Assert.assertEquals(5, favoritesList.size)
        sqlite.clearFavorites()
        val newFavoritesList = sqlite.getFavorites()
        Assert.assertEquals(0, newFavoritesList.size)
    }
}