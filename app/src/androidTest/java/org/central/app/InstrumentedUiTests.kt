package org.central.app

import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.action.ViewActions.typeText
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import org.central.app.Utils.atPosition
import org.junit.Rule


/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class InstrumentedUiTests {
    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getTargetContext()
        assertEquals("org.central.app", appContext.packageName)
    }

    @Rule @JvmField
    var parentActivityRule = ActivityTestRule<ParentActivity>(ParentActivity::class.java)

    @Test
    fun launch_app_check_toolbar() {
        parentActivityRule.activity
        onView(withId(R.id.parent_toolbar)).check(matches(isDisplayed()))
    }

    @Test
    fun launch_app_search_for_hitchhikers_guide() {
        parentActivityRule.activity


        onView(withId(R.id.parent_toolbar_search)).perform(click())
        onView(withId(R.id.search_edit_text)).perform(typeText("hi"))
        onView(withId(R.id.search_submit_button)).perform(click())
        onView(withId(R.id.search_results_item_list))
            .check(matches(atPosition(0, hasDescendant(withText("The Hitchhiker's Guide To The Galaxy (Book)")))))
    }

    @Test
    fun launch_app_search_for_biggie() {
        parentActivityRule.activity

        onView(withId(R.id.parent_toolbar_search)).perform(click())
        onView(withId(R.id.search_edit_text)).perform(typeText("notorious"))
        onView(withId(R.id.search_submit_button)).perform(click())
        onView(withId(R.id.search_results_item_list))
            .check(matches(atPosition(0, hasDescendant(withText("The Notorious B.I.G. (Music)")))))
    }

    @Test
    fun launch_app_search_for_lost_boys() {
        parentActivityRule.activity

        onView(withId(R.id.parent_toolbar_search)).perform(click())
        onView(withId(R.id.search_edit_text)).perform(typeText("lost boys"))
        onView(withId(R.id.search_submit_button)).perform(click())
        onView(withId(R.id.search_results_item_list))
            .check(matches(atPosition(0, hasDescendant(withText("The Lost Boys (1987 Movie)")))))
    }
}
