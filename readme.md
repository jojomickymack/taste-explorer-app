# Taste Explorer

This app is my capstone project for the [coursera android specializeation](https://www.coursera.org/learn/aadcapstone) by 
[Vanderbilt University](https://www.vanderbilt.edu/).

This app uses [tastedive's](https://tastedive.com/) rest api for music, movie, and book recommendations based on what you search for. 
Specifically, I am hitting [an autocomplete api](https://tastedive.com/api/autocomplete?v=3&t=&target=search&q=no) 
and a [suggestions api](https://tastedive.com/api/similar?info=1&q=The-Restaurant-At-The-End-Of-The-Universe).

This app has only a few activities, favoring swapping out fragments in a single 'parent activity' to update the ui, and it's written in 
kotlin. Kotlin is an official android language now - see [this link](https://developer.android.com/kotlin/) for more information about it. The 
language was created by Jetbrains, the creators of Android Studio, and much of that IDE is written in Kotlin. Official Android documentation 
offers example code in both kotlin and java now since kotlin has become so ubiquitous.

## Overview of TasteExplorer's Components

When you search for items, you can select the item you want to view from the search results. Items will be listed with the 'similar items' 
tastedive returns. Selecting a 'similar item' will bring up the 'item detail' view, which includes a thumbnail image will be fetched from 
youtube (if it's available) for the item you selected, and a short caption from wikipedia.

In the bottom toolbar, you can tap the 'star' button to store the item in your 'favorites' database. There is also a 'search' button in the 
bottom toolbar which will generate a new list of similar items. If the youtube and wikipedia links were not 'null' in the response from tastedive, there will be buttons to open a video in the youtube app, or go to the wikipediia article in your 
browser of choice. 

- Activities - there is a parent activity and an activity that 'welcomes' you = swipe to the left to page through it's fragments.

- Fragments - the search page, the list of results, and the item detail pages are all fragments

- AsyncTask - the SearchResults and ItemDetail fragments load data from an api using AsyncTask and there's a spinner displayed briefly while 
that happens. RecyclerLists are updated once the response is received - also an image is downloaded if it's available for display in the 
ItemDetail Toolbar

- ContentProvider - when you select an item from the search results or related item, there's a 'star' button in the toolbar. Clicking it 
inserts the item you liked into the 'favorites' database. You can navigate to the 'favorites' page to view items you've saved, and pull up 
their item detail page by selecting them.

- Service - the music playing in the background is a Service running a MediaPlayer

- BroadcastReveiver - from the settings, click the 'start' and 'stop' buttons to play/pause the music.

- BroadcastIntent - in the welcome pages, you can click on a button to open a browser and go to the tastedive signup page.

## Code Highlights For Review

Since it's unlikely you're going to actually build and test this, and because reading the code online is much easier and accessible in gitlab, 
I've provided some links to specific files that relate to the requirements. 

The files below are the content provider - it's for storing favorite items. 

[https://gitlab.com/jojomickymack/taste-explorer-app/tree/master/app/src/main/java/org/central/app/sqliteSupport](https://gitlab.com/jojomickymack/taste-explorer-app/tree/master/app/src/main/java/org/central/app/sqliteSupport)

below is a service that plays music in the background - it has a BroadcastReveiver. 

[https://gitlab.com/jojomickymack/taste-explorer-app/blob/master/app/src/main/java/org/central/app/services/MusicPlayingService.kt](https://gitlab.com/jojomickymack/taste-explorer-app/blob/master/app/src/main/java/org/central/app/services/MusicPlayingService.kt)

I used a lot of AsyncTask to do an http request and update the ui once it's loaded - there's two in the ItemDetail page - one to get the json 
response and one to download a picture of the item it it's available.

[https://gitlab.com/jojomickymack/taste-explorer-app/blob/master/app/src/main/java/org/central/app/pages/searchSystem/ItemDetail.kt](https://gitlab.com/jojomickymack/taste-explorer-app/blob/master/app/src/main/java/org/central/app/pages/searchSystem/ItemDetail.kt)

Unit tests are here for testing the HttpController and JsonController, which don't need the app running in order to function.

[https://gitlab.com/jojomickymack/taste-explorer-app/tree/master/app/src/test/java/org/central/app](https://gitlab.com/jojomickymack/taste-explorer-app/tree/master/app/src/test/java/org/central/app) 

There are also instrumentation tests that use espresso - the SqliteController has tests, and the search functionality has tests as well.

[https://gitlab.com/jojomickymack/taste-explorer-app/tree/master/app/src/androidTest/java/org/central/app](https://gitlab.com/jojomickymack/taste-explorer-app/tree/master/app/src/androidTest/java/org/central/app) 

## Notes

The app was created from the Android Studio 'new project template'. It uses gradle 5.1.1, which is the lowest version Android Studio wants to 
support now.

I used minSdk 22 and target sdk 28 (my physical device is android P). Compiled sdk is 28, and I am using app-compat to hopefully be able to 
run smoothly on older devices. 

If you can't run this on your devices because of the target sdk, you might just change the target and compile sdk to a lower number. 
 
